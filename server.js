//var fs = require('fs');
var http = require('http');
var https = require('https');
//var privateKey = fs.readFileSync('./sslcert/server.key', 'utf8');
//var certificate = fs.readFileSync('./sslcert/server.crt', 'utf8');
const express = require('express');
const app = express();
var bodyParser = require('body-parser');

//var credentials = { key: privateKey, cert: certificate };

const cors = require('cors')
const corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200
}
app.use(cors(corsOptions))
app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())

app.use(express.json())

let router = require('./routers/s3.router');
app.use('/', router);

// Create a Server

// var httpServer = http.createServer(app);
// var httpsServer = https.createServer(credentials, app);

//httpServer.listen(8080);
//httpsServer.listen(8710);

const server = app.listen(80, function() {

    let host = server.address().address
    let port = server.address().port

    console.log("App listening at http://%s:%s", host, port);
})