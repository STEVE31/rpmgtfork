let express = require('express');
let router = express.Router();

const emitter = require('../controllers/emitters');
let upload = require('../config/multer.config');

const awsWorker = require('../controllers/controller');

const mongoworker = require('../controllers/mongocontroller');

router.post('/api/file/save', upload.single("file"), awsWorker.doUpload);

router.get('/api/file/myfiles', awsWorker.listKeyNames);

router.get('/api/file/:filename', awsWorker.doDownload);

router.delete('/api/file/:filename', awsWorker.doDelete);

router.get('/api/mongo/connectionstatus', mongoworker.doConnect);

router.post('/api/mongo/writetodb', mongoworker.doWrite);

router.get('/api/mongo/readuserlist', mongoworker.doReadUsers);

router.get('/api/mongo/doesuserexist', mongoworker.doExistUser);

router.get('/api/mongo/userconfirmation', mongoworker.doConfirmUser);

router.post('/api/mongo/userupdatedetails', mongoworker.doUpdateUser);

router.get('/api/mongo/eventemitterforchanges', emitter.eventEmitter);

router.get('/', (request, response) => {
    response.send("We're good!");
});

module.exports = router;