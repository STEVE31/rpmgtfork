const EventEmitter = require('events');

const Stream = new EventEmitter();
const { MongoClient, ObjectId } = require('mongodb');

const uri = "mongodb://<username>:<pwd>@<ip>:<port>/<authdb>";

const client = new MongoClient(uri, { useUnifiedTopology: true });

exports.eventEmitter = (req, res) => {
    if ((Object.keys(req.query).length === 0)) {
        res.send('we need some parameters')
    } else {
        res.writeHead(200, {
            'Content-Type': 'text/event-stream',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive'
        });
        send(req, res);
    }
}

function send(req, res) {
    Stream.on('push', function(event, data) {
        res.write('event: ' + String(event) + '\n' + 'data:' + JSON.stringify(data) + '\n\n');
    });
    console.log(res.finished);

    function monitorListingsUsingEventEmitter(client, timeInMs = 60000, pipeline = [
        { $match: { owner: req.params.id } }
    ]) {
        try {
            client.connect();
            const collection = client.db("reap").collection("cart");
            const changeStream = collection.watch(pipeline);
            console.log('checking db')
            Stream.emit('push', 'message', { msg: 'checking db' });
            var moon = setInterval(() => {
                Stream.emit('push', 'message', { msg: 'checking db' });
            }, 40000)
            changeStream.on('change', (next) => {
                //console.log(next);
                Stream.emit('push', 'message', { msg: next });
                //res.write('event: ' + String(event) + '\n' + 'data:' + JSON.stringify(next) + '\n\n');
                //res.write(next);
            }, err => {
                console.log(err);
            });
            closeChangeStream2(changeStream, moon, res);
            req.on("end", () => {
                console.log('req ended');
                closeChangeStream(changeStream, moon);
            });
            req.on("close", () => {
                console.log('req closed');
                closeChangeStream(changeStream, moon);
            });
        } catch (e) {
            res.send(e);
        }
    }
    monitorListingsUsingEventEmitter(client);

    function closeChangeStream(changeStream, moon) {
        return new Promise((resolve) => {
            setTimeout(() => {
                clearInterval(moon);
                console.log("Closing the change stream");
                changeStream.close();
                resolve();
            }, 500);
        })
    };

    function closeChangeStream2(changeStream, moon, res) {
        return new Promise((resolve) => {
            setTimeout(() => {
                clearInterval(moon);
                console.log("Closing the change stream2");
                changeStream.close();
                resolve();
            }, 3600000);
        })
    };
    //3600000
}