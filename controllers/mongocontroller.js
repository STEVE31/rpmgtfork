const { MongoClient, ObjectId } = require('mongodb');

const uri = "mongodb://usename:password@<ipaddress>/<database>";

const client = new MongoClient(uri);

var databasenames = [];

exports.doConnect = (req, res) => {
    async function Connecting() {
        try {
            await client.connect();

            await listDatabases(client);
            await client.db().admin().listDatabases();

            console.log("Databases:");
            console.log(req.body);
            databasesList.databases.forEach(db => databasenames.push(` - ${db.name}`));
            res.send("connection made" + databasenames);
            // await createListing(client, {
            //     name: "Lovely Loft",
            //     summary: "A charming loft in Paris",
            //     bedrooms: 1,
            //     bathrooms: 1
            // });
        } catch (e) {
            console.error(e);
            res.send("connection not made");
        } finally {
            await client.close();
            res.send("connection closed");
        }
    }
    Connecting();
}

exports.doWrite = (req, res) => {
    console.log("The params" + `${req.params}`);
    if (!(Object.keys(req.body).length === 0) && !(Object.keys(req.query).length === 0)) {
        //res.send(req.body);
        //res.send(req.query);
        async function Connecting() {
            try {
                await client.connect();

                //await listDatabases(client);
                //await client.db().admin().listDatabases();

                //console.log("Databases:");
                //console.log(req.body);
                //databasesList.databases.forEach(db => databasenames.push(` - ${db.name}`));
                await createListing(client, req.body, req.query.dbname);
                await res.send("written to " + req.query.dbname);
            } catch (e) {
                await console.error(e);
                await res.send("write not done due to " + `${e}`);
            } finally {
                //await client.close();
                await console.log("connection closed");
            }
        }
        Connecting();
    } else {
        res.send("you need parameters");
    }
}

exports.doReadUsers = (req, res) => {
    if (!(Object.keys(req.query).length === 0)) {
        async function Connecting() {
            try {
                await client.connect();
                cursor = await client.db(req.query.dbname).collection(req.query.collection)
                    .find({ username: { $gte: '' }, password: { $gte: '' } });
                const result = await cursor.toArray();
                if (result && result.length > 0) {
                    await res.send(result);
                } else {
                    await res.send("No listings found");
                }
            } catch (e) {
                res.send(`could not retrieve results due to '${e}'`);
            } finally {
                //await client.close();
                console.log("connection closed");
            }
        }
        Connecting();
    } else {
        res.send("you need parameters");
    }
}

exports.doConfirmUser = (req, res) => {
    if (!(Object.keys(req.query).length === 0)) {
        async function Connecting() {
            try {
                await client.connect();
                result = await client.db(req.query.dbname).collection(req.query.collection)
                    .findOne({ emailaddress: req.query.email, password: req.query.password });

                if (result) {
                    console.log(`Found a listing in the collection with '${req.query.email}'.`);
                    res.send(result);
                } else {
                    console.log(`No listings found with the name '${req.query.email}'`);
                    res.send("Incorrect login details");
                }
            } catch (e) {
                res.send(`could not retrieve results due to '${e}'`);
            } finally {
                //await client.close();
                console.log("connection closed");
            }
        }
        Connecting();
    } else {
        res.send("you need parameters");
    }
}

exports.doExistUser = (req, res) => {
    if (!(Object.keys(req.query).length === 0)) {
        async function Connecting() {
            try {
                await client.connect();
                result = await client.db(req.query.dbname).collection(req.query.collection)
                    .findOne({ emailaddress: req.query.email });

                if (result) {
                    console.log(`Found a listing in the collection with the email '${req.query.email}':`);
                    console.log(result);
                    res.send("Yes. User does exist");
                } else {
                    console.log(`No listings found with the name '${req.query.email}'`);
                    res.send("No. User does not exist");
                }
            } catch (e) {
                res.send(`could not retrieve results due to '${e}'`);
            } finally {
                //await client.close();
                console.log("connection closed");
            }
        }
        Connecting();
    } else {
        res.send("you need parameters");
    }
}

exports.doUpdateUser = (req, res) => {
    if (!(Object.keys(req.body).length === 0) && !(Object.keys(req.query).length === 0)) {
        async function Connecting() {
            try {
                await client.connect();
                var g = req.query.id;
                result = await client.db(req.query.dbname).collection(req.query.collection)
                    .updateOne({ _id: ObjectId(req.query.id) }, { $set: req.body });

                console.log(`${result.matchedCount} document(s) matched the query criteria.`);
                console.log(`${result.modifiedCount} document(s) was/were updated.`);
                if (result.matchedCount > 0) {
                    console.log(`${result.matchedCount} document(s) matched the query criteria.`);
                    res.send("Updated");
                } else {
                    res.send("Record not found");
                }
            } catch (e) {
                res.send(`could not retrieve results due to '${e}'`);
            } finally {
                //await client.close();
                console.log("connection closed");
            }
        }
        Connecting();
    } else {
        res.send("you need parameters");
    }
}

async function listDatabases(client) {
    databasesList = await client.db().admin().listDatabases();

    console.log("Databases:");
    databasesList.databases.forEach(db => console.log(` - ${db.name}`));
}

async function createListing(client, newListing, dbname) {
    const result = await client.db(dbname).collection("listingsAndReviews").insertOne(newListing);
    console.log(`New listing created with the following id: ${result.insertedId}`);
}

async function findOneListingByName(client, nameOfListing, dbname, collection) {
    result = await client.db(dbname).collection(collection)
        .findOne({ name: nameOfListing });

    if (result) {
        console.log(`Found a listing in the collection with the name '${nameOfListing}':`);
        console.log(result);
    } else {
        console.log(`No listings found with the name '${nameOfListing}'`);
    }
}

async function findOneListingByName(client, nameOfListing, dbname, collection, myname, pwd) {
    result = await client.db(dbname).collection(collection)
        .findOne({ name: myname, password: pwd });

    if (result) {
        console.log(`Found a listing in the collection with the name '${nameOfListing}':`);
        console.log(result);
    } else {
        console.log(`No listings found with the name '${nameOfListing}'`);
    }
}